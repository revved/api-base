import bodyParser from "body-parser";
import compression from "compression";
import cors from "cors";

import { errorHandler, handleNotFoundRoute } from "./responseHandlers";
import SwaggerValidator from "./SwaggerValidator";

export async function configureMiddleware(app, opts) {
    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({ extended: true }));

    // parse application/json
    app.use(bodyParser.json());

    // setup compression middleware
    app.use(compression());

    // setup cors
    app.use(cors({ credentials: true }));
    app.options("*", cors());
}

export function configureDefaultErrorHandler(app) {
    app.use(errorHandler);
}

export function configureNotFoundEndpoints(app) {
    // All other routes should result in an error
    app.all("*", handleNotFoundRoute);
}

export function getSwaggerValidationMiddlewareForRequest(method, path) {
    const specValMiddleware = SwaggerValidator.getValidationMiddleware(
        method,
        path
    );

    return specValMiddleware;
}
