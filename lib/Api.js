import express from "express";
import forEach from "lodash/forEach";

import Logger from "./Logger";
import ErrorHelper from "./ErrorHelper";
import { wrapControllerFn } from "./responseHandlers";
import SwaggerValidator from "./SwaggerValidator";
import {
    configureMiddleware,
    configureDefaultErrorHandler,
    configureNotFoundEndpoints,
    getSwaggerValidationMiddlewareForRequest
} from "./apiMiddlewareHelpers";

export default class Api {
    constructor() {
        this.app = express();
    }

    async configure(apiConfig) {
        Api.validateConfig(apiConfig);

        this.env = apiConfig.env;
        this.name = apiConfig.name;
        this.port = apiConfig.port;
        this.shouldStdOut = apiConfig.shouldStdOut;

        // Init logger
        await Logger.initConfig({
            env: this.env,
            name: this.name,
            shouldStdOut: this.shouldStdOut
        });

        // Define any custom errors if applicable
        const errorDef = apiConfig.errorDefinitions;
        ErrorHelper.defineCustomErrors(errorDef);

        // IMPORTANT: The order here is very important
        SwaggerValidator.config(apiConfig.swagger);

        // Configgure middleware
        this.configureMiddleware(apiConfig);

        // Register endpoints
        this.registerEndpoints(apiConfig);

        // Configure the default error handler
        configureDefaultErrorHandler(this.app);
    }

    // eslint-disable-next-line class-methods-use-this
    shutDown() {
        const logger = Logger.getInstance();
        logger.end();
    }

    async configureMiddleware(apiConfig) {
        await configureMiddleware(this.app, apiConfig);
    }

    registerEndpoints(apiConfig) {
        const { endpoints } = apiConfig;

        forEach(endpoints, endpoint => {
            this.registerEndpoint(
                apiConfig,
                endpoint.method,
                endpoint.path,
                endpoint.handler
            );
        });

        // All other requests should throw not found errors
        configureNotFoundEndpoints(this.app);
    }

    registerEndpoint(apiConfig, method, path, controllerFn) {
        const wrapper = wrapControllerFn(controllerFn);
        const valMiddleware = getSwaggerValidationMiddlewareForRequest(
            method,
            path
        );
        this.app[method](path, valMiddleware, wrapper);
    }

    static validateConfig(apiConfig) {
        if (!apiConfig.name) {
            throw new Error("`apiConfig` requires `name` to be passed.");
        }
    }
}
