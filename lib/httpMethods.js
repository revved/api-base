import methods from "methods";
import forEach from "lodash/forEach";
import toUpper from "lodash/toUpper";

const httpMethods = {};
forEach(methods, method => {
    httpMethods[toUpper(method)] = method;
});

export default httpMethods;
