import { OpenApiValidator } from "express-openapi-validate";
import replace from "lodash/replace";
import split from "lodash/split";
import forEach from "lodash/forEach";
import startsWith from "lodash/startsWith";
import join from "lodash/join";
import toLower from "lodash/toLower";

class SwaggerValidator {
    config(swagger) {
        this.swagger = swagger;
    }

    getValidationMiddleware(method, path) {
        const validator = new OpenApiValidator(this.swagger);
        const newMethod = toLower(method);
        const newPath = this.convertColonToCurlyBraces(path);
        const specValMiddleware = validator.validate(newMethod, newPath);

        return specValMiddleware;
    }

    validateResponse(req, statusCode, bodyResponse, headers) {
        const { method } = req;
        const { path } = req.route; // Use this as req.path will have resolved params
        const validator = new OpenApiValidator(this.swagger);
        const newMethod = toLower(method);
        const newPath = this.convertColonToCurlyBraces(path);
        const validateResponse = validator.validateResponse(newMethod, newPath);

        let error;

        try {
            validateResponse({
                body: bodyResponse,
                statusCode,
                headers: headers || {}
            });
        } catch (e) {
            error = e;
            error.isResponseError = true;
        }

        return error;
    }

    convertColonToCurlyBraces(path) {
        const pathParts = split(path, "/");
        const newPathParts = [];

        forEach(pathParts, pathPart => {
            let newPathPart = pathPart;
            if (startsWith(pathPart, ":")) {
                newPathPart = replace(pathPart, ":", "");
                newPathPart = `{${newPathPart}}`;
            }
            newPathParts.push(newPathPart);
        });
        const newPath = join(newPathParts, "/");
        return newPath;
    }
}

export default new SwaggerValidator();
