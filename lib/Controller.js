import autoBind from "auto-bind";

export default class Controller {
    constructor(options) {
        this.application = options.application;
        this.mapper = options.mapper;

        autoBind(this);
    }

    async get(req) {
        const id = this.mapper.getId(req);
        const resp = await this.application.get(id);
        return resp;
    }

    async create(req) {
        const newObj = this.mapper.getNewObject(req);
        const resp = await this.application.create(newObj);
        return resp;
    }

    async update(req) {
        const id = this.mapper.getId(req);
        const updateObj = this.mapper.getUpdateObject(req);
        const resp = await this.application.update(id, updateObj);
        return resp;
    }

    async delete(req) {
        const id = this.mapper.getId(req);
        const resp = await this.application.delete(id);
        return resp;
    }
}
