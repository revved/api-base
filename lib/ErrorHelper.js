import forEach from "lodash/forEach";
import HTTPStatus from "http-status";
import get from "lodash/get";
import isNumber from "lodash/isNumber";
import cammelCase from "lodash/camelCase";
import upperFirst from "lodash/upperFirst";
import replace from "lodash/replace";
import bunyan from "bunyan";
import { BaseError } from "@revved/lib-error-tools";
import { ValidationError } from "express-openapi-validate";

class ErrorHelper {
    constructor() {
        this.customErrors = {};
    }

    createErrorResponse(error) {
        const errResp = {
            code: error.code || "InternalServer",
            message: error.displayMessage || "Oops! Something went wrong...",
            statusCode: error.statusCode || HTTPStatus.INTERNAL_SERVER_ERROR,
            level: error.level || bunyan.FATAL
        };

        const debugMessage = get(error, "debug.message") || error.message;
        const debugContext = get(error, "debug.context") || {};
        errResp.debug = {
            message: debugMessage,
            context: debugContext
        };

        // Handle speacial case for spec validation error
        // See: https://github.com/Hilzu/express-openapi-validate#class-validationerror-extends-error
        if (error instanceof ValidationError) {
            if (error.isResponseError) {
                errResp.code = "ResponseSpecValidation";
                errResp.statusCode = HTTPStatus.INTERNAL_SERVER_ERROR;
            } else {
                errResp.code = "BadRequestSpecValidation";
            }
            errResp.debug.context = { errors: error.data };
        }

        return errResp;
    }

    defineCustomErrors(errorDefinitions) {
        // Define default errors
        this.defineDefaultErrorDefinitions();

        forEach(errorDefinitions, errorDef => {
            this.defineCustomError(errorDef);
        });
    }

    defineCustomError(errorDef) {
        const code = replace(errorDef.code, "Error", "");
        const className = upperFirst(cammelCase(`${code}Error`));

        this.customErrors[className] = class extends BaseError {
            constructor(options) {
                const defaults = {
                    message: errorDef.message,
                    code: errorDef.code,
                    displayMessage: errorDef.displayMessage,
                    statusCode: errorDef.statusCode,
                    level: errorDef.level,
                    debug: errorDef.debug
                };

                super(options, defaults);
            }
        };
    }

    defineDefaultErrorDefinitions() {
        forEach(HTTPStatus, (name, statusCode) => {
            const parsedStatusCode = Number(statusCode);

            if (isNumber(parsedStatusCode) && parsedStatusCode >= 400) {
                const errorDef = {
                    message: HTTPStatus[statusCode],
                    code: upperFirst(cammelCase(name)),
                    displayMessage: HTTPStatus[`${statusCode}_MESSAGE`],
                    statusCode,
                    level: parsedStatusCode < 500 ? bunyan.ERROR : bunyan.FATAL
                };
                this.defineCustomError(errorDef);
            }
        });
    }
}

export default new ErrorHelper();
