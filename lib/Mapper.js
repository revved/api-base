import autoBind from "auto-bind";
import uuid from "uuid/v4";
import moment from "moment";

export default class Mapper {
    constructor() {
        autoBind(this);
    }

    // Get from req

    getId(req) {
        return req.params.id;
    }

    getNewObject(req) {
        const newObj = req.body;
        newObj.id = this.createIdForNewObject(newObj);
        newObj.created_at = this.getNowTs();
        newObj.updated_at = this.getNowTs();

        return newObj;
    }

    getUpdateObject(req) {
        const updateObj = req.body;
        updateObj.updated_at = this.getNowTs();
        return updateObj;
    }

    // Create data

    createUuid() {
        return uuid();
    }

    createIdForNewObject(newObject) {
        return this.createUuid();
    }

    getNowTs() {
        return moment().unix();
    }
}
