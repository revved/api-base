import HTTPStatus from "http-status";
import Logger from "./Logger";
import ErrorHelper from "./ErrorHelper";
import SwaggerValidator from "./SwaggerValidator";

export function wrapControllerFn(controllerFn) {
    return async function wrapper(req, res, next) {
        try {
            const response = await controllerFn(req);
            return successHandler(response, req, res, next);
        } catch (error) {
            return errorHandler(error, req, res, next);
        }
    };
}

function successHandler(response, req, res, next) {
    // Before sending response back, validate it against the spec
    const error = SwaggerValidator.validateResponse(
        req,
        HTTPStatus.OK,
        response
    );
    // If there's a validation error handle validation error
    if (error) {
        return errorHandler(error, req, res, next);
    }

    return res.send(response);
}

export function errorHandler(error, req, res, next) {
    const logger = Logger.getInstance();
    logger.logError(error, req, res);

    const errorResponse = ErrorHelper.createErrorResponse(error);

    return res.status(errorResponse.statusCode).send(errorResponse);
}

export function handleNotFoundRoute(req, res, next) {
    const error = new ErrorHelper.customErrors.NotFoundError(
        `Path not found: ${req.path}`
    );
    return errorHandler(error, req, res, next);
}
