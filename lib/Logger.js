import assignIn from "lodash/assignIn";
import bunyan from "bunyan";
import LogzioBunyanStream from "logzio-bunyan";
import PrettyStream from "bunyan-prettystream";
import { SecretManager } from "@revved/lib-secret-manager";

let logger;
let config;

// IMPORTANT: this name is use for all envs, but each have their own aws account
const LOGZ_IO_TOKEN_SECRET_MANAGER_KEY = "logz.io_token";

export default class Logger {
    constructor(configLocal) {
        const loggerOptions = {
            token: configLocal.token
        };

        this.logzioStream = new LogzioBunyanStream(loggerOptions);

        const log = bunyan.createLogger({
            name: configLocal.name,
            serializers: {
                req: bunyan.stdSerializers.req, // TODO: remove auth header
                res: bunyan.stdSerializers.res,
                err: bunyan.stdSerializers.err
            },
            streams: this.getStreams(configLocal)
        });

        // Extend bunyan logger
        const loggerLoc = assignIn(this, log);

        return loggerLoc;
    }

    end() {
        this.logzioStream.end();
    }

    // Uses the error level to log appropietly: .error, vs .info, etc
    logError(error, req, res) {
        try {
            this.logErrorNotCatching(error, req, res);
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }
    }

    logErrorNotCatching(error, req, res) {
        const logEntry = {
            err: error,
            req,
            res
        };

        const level = error.level || bunyan.FATAL;
        const levelName = bunyan.nameFromLevel[level];

        this[levelName](logEntry);
    }

    getStreams(configLocal) {
        const streams = [this.getLogzIoStream()];
        if (configLocal.shouldStdOut) {
            streams.push(this.getStdOutStream());
        }

        return streams;
    }

    getLogzIoStream() {
        return {
            type: "raw",
            level: "warn",
            stream: this.logzioStream
        };
    }

    // eslint-disable-next-line class-methods-use-this
    getStdOutStream() {
        const prettyStdOut = new PrettyStream();
        prettyStdOut.pipe(process.stdout);

        return {
            level: "debug",
            stream: prettyStdOut
        };
    }

    static getInstance() {
        if (!config) {
            throw new Error("Logger.init needs to be called before this");
        }

        if (!logger) {
            logger = new Logger(config);
        }

        return logger;
    }

    static async initConfig(configLocal) {
        config = configLocal;
        const token = await Logger.getLogzIoToken(configLocal.env);
        config.token = token;
    }

    static async getLogzIoToken(env) {
        const token = await SecretManager.getSecret(
            env,
            LOGZ_IO_TOKEN_SECRET_MANAGER_KEY
        );
        return token;
    }
}
