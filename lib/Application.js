import autoBind from "auto-bind";
import merge from "lodash/merge";

export default class Application {
    constructor(options) {
        this.database = options.database;
        autoBind(this);
    }

    async get(id) {
        const objc = this.database.get(id);
        return objc;
    }

    async create(newObj) {
        const createdObj = await this.database.create(newObj);
        return createdObj;
    }

    async update(id, updateObj) {
        const currentObj = await this.database.get(id);
        const updatedObj = merge(currentObj, updateObj);

        await this.database.update(id, updatedObj);
        return updatedObj;
    }

    async delete(id) {
        const deletedObj = await this.database.delete(id);
        return deletedObj;
    }
}
