/* eslint-disable no-empty-function */
import autoBind from "auto-bind";
import ErrorHelper from "./ErrorHelper";

const store = {};

export default class Database {
    constructor(options) {
        autoBind(this);
    }

    // Defaults

    async get(id) {
        const obj = store[id];
        if (!obj) {
            throw new ErrorHelper.customErrors.NotFoundError(
                `User ${id} does not exist`
            );
        }
        return obj;
    }

    async create(newObj) {
        store[newObj.id] = newObj;
        return newObj;
    }

    async update(id, updatedObj) {
        store[id] = updatedObj;
        return updatedObj;
    }

    async delete(id) {
        const currentObj = await this.get(id);
        delete store[id];
        return currentObj;
    }
}
