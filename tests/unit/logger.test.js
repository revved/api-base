import { Logger } from "../../lib";

test("should try to get logger instance before config init", async () => {
    // const apiConfig = {};
    let err;
    try {
        const logger = Logger.getInstance();
    } catch (e) {
        err = e;
    }
    expect(err).toBeDefined();
    expect(err.message).toBe("Logger.init needs to be called before this");
});

test("should create logger that doesn't stdOut", async () => {
    const config = {
        env: "dev",
        name: "a-test",
        shouldStdOut: false
    };
    await Logger.initConfig(config);
    const logger = Logger.getInstance();
    expect(logger).toBeDefined();
    logger.end();
});

test("should not throw when logError fails internally", async () => {
    const config = {
        env: "dev",
        name: "a-test"
    };
    await Logger.initConfig(config);
    const logger = Logger.getInstance();

    // Send no params so it blows up inside
    const didLog = logger.logError();
    expect(didLog).toBe(false);
    logger.end();
});
