import { Api } from "../../lib";

test("should create an api with no test", async () => {
    const api = new Api();
    const apiConfig = { env: "dev" };
    let err;
    try {
        await api.configure(apiConfig);
    } catch (e) {
        err = e;
    }

    expect(err).toBeDefined();
    expect(err.message).toBe("`apiConfig` requires `name` to be passed.");
});
