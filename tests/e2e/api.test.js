import HTTPStatus from "http-status";
import clone from "lodash/clone";

import { Api, httpMethods } from "../../lib";
import {
    makePostRequest,
    makeGetRequest,
    makePutRequest,
    makeDeleteRequest,
    makeOptionsRequest,
    getDefaultApiConfig
} from "../testHelpers";

let api;
// const ts = new Date().valueOf();

beforeAll(async () => {
    api = new Api();
    const apiConfig = await getDefaultApiConfig();
    apiConfig.shouldStdOut = true;
    await api.configure(apiConfig);
});

afterAll(async () => {
    api.shutDown();
});

test("should create and get a user", async () => {
    // Make request and assert response
    const newUser = getMockUser();

    // Create
    const resp = await makePostRequest(api.app, "/users", newUser);
    const createdUser = clone(resp.body);
    console.log("resp", resp.headers);

    // Assert resp
    expect(resp.statusCode).toBe(HTTPStatus.OK);
    expect(resp.body).toBeDefined();
    expect(resp.body.id).toBeDefined();
    delete resp.body.id;
    delete resp.body.created_at;
    delete resp.body.updated_at;
    expect(resp.body).toEqual(newUser);

    // Get
    const respB = await makeGetRequest(api.app, `/users/${createdUser.id}`);
    const retrievedUser = respB.body;
    // Assert resp
    expect(respB.statusCode).toBe(HTTPStatus.OK);
    expect(respB.body).toBeDefined();
    expect(respB.body.id).toBeDefined();

    // Assert that created and retrieved match
    expect(createdUser).toEqual(retrievedUser);
});

test("should create and update a user", async () => {
    // Make request and assert response
    const newUser = getMockUser();

    // Create
    const resp = await makePostRequest(api.app, "/users", newUser);
    expect(resp.statusCode).toBe(HTTPStatus.OK);
    const createdUser = resp.body;

    // Update
    const userUpdate = {
        first_name: "Addison"
    };
    const respB = await makePutRequest(
        api.app,
        `/users/${createdUser.id}`,
        userUpdate
    );

    // Assert updated and not updated props
    const updatedUser = respB.body;
    expect(resp.statusCode).toBe(HTTPStatus.OK);
    expect(updatedUser.first_name).toEqual("Addison");
    expect(updatedUser.last_name).toEqual("Rivero");
});

test("should create and delete a user", async () => {
    // Make request and assert response
    const newUser = getMockUser();

    // Create
    const resp = await makePostRequest(api.app, "/users", newUser);
    expect(resp.statusCode).toBe(HTTPStatus.OK);
    const createdUser = resp.body;

    // Delete
    const respB = await makeDeleteRequest(api.app, `/users/${createdUser.id}`);
    const deletedUser = respB.body;
    expect(createdUser).toEqual(deletedUser);
});

test("should throw error getting user that doesn't exist", async () => {
    // Make request and assert response
    const id = "12345678";
    const respB = await makeGetRequest(api.app, `/users/${id}`);

    // Assert
    expect(respB.statusCode).toBe(HTTPStatus.NOT_FOUND);
    expect(respB.body.code).toBe("NotFound");
    expect(respB.body.message).toBe("You seem to be lost!");
    expect(respB.body.debug.message).toBe(`User ${id} does not exist`);
});

test("should make a post request to an endpoint that doesn't exist and result in a not found error", async () => {
    // Make request and assert response
    const reqBody = { first_name: "manuel", last_name: "rivero" };
    const path = "/non/existing";
    const resp = await makePostRequest(api.app, path, reqBody);

    // Assert
    expect(resp.statusCode).toBe(HTTPStatus.NOT_FOUND);
    expect(resp.body).toBeDefined();
    expect(resp.body.message).toBe("You seem to be lost!");
    expect(resp.body.code).toBe("NotFound");
    expect(resp.body.debug.message).toBe(`Path not found: ${path}`);
});

test("should make a post request without required properties and result in a validation error", async () => {
    // Make request and assert response
    const newUser = {
        first_name: "Manuel"
    };
    const resp = await makePostRequest(api.app, "/users", newUser);

    expect(resp.statusCode).toBe(HTTPStatus.BAD_REQUEST);
    expect(resp.body).toBeDefined();
    expect(resp.body.message).toBe("Oops! Something went wrong..."); // Generic friendly error
    expect(resp.body.code).toBe("BadRequestSpecValidation");
    expect(resp.body.debug.message).toBe(
        "Error while validating request: request.body should have required property 'last_name'"
    );
    expect(resp.body.debug.context.errors).toBeDefined();
    expect(resp.body.debug.context.errors).toHaveLength(1);
});

test("should make a get request to endpoint that always return internal server error", async () => {
    const resp = await makeGetRequest(api.app, `/users/random`);

    expect(resp.statusCode).toBe(HTTPStatus.INTERNAL_SERVER_ERROR);
    expect(resp.body).toBeDefined();
    expect(resp.body.message).toBe("Oops! Something went wrong...");
    expect(resp.body.code).toBe("InternalServer");
    expect(resp.body.debug.message).toBe("Random error!"); // Comes from testHelpers
    expect(resp.body.debug.context).toEqual({});
});

test("should make a get request to endpoint that returns a custom error", async () => {
    const resp = await makeGetRequest(api.app, `/users/custom`);

    expect(resp.statusCode).toBe(HTTPStatus.CONFLICT);
    expect(resp.body).toBeDefined();
    expect(resp.body.message).toBe("Custom error!");
    expect(resp.body.code).toBe("Custom");
    expect(resp.body.debug.message).toBe("A custom error"); // Comes from testHelpers
    expect(resp.body.debug.context).toEqual({});
});

test("should make a get request to endpoint that returns a response validation error", async () => {
    const resp = await makeGetRequest(api.app, `/users/resp-validation`);
    expect(resp.statusCode).toBe(HTTPStatus.INTERNAL_SERVER_ERROR);
    expect(resp.body).toBeDefined();
    expect(resp.body.message).toBe("Oops! Something went wrong...");
    expect(resp.body.code).toBe("ResponseSpecValidation");
    expect(resp.body.debug.message).toBe(
        "Error while validating response: response.body should have required property 'created_at'"
    );
    expect(resp.body.debug.context.errors).toBeDefined();
    expect(resp.body.debug.context.errors[0]).toBeDefined();
    expect(resp.body.debug.context.errors[0].message).toBe(
        "should have required property 'created_at'"
    );
});

test("should get cors headers", async () => {
    // Make request and assert response
    const newUser = getMockUser();

    const optsResp = await makeOptionsRequest(api.app, "/users", newUser);
    expect(optsResp.statusCode).toBe(HTTPStatus.NO_CONTENT);
    expect(optsResp.body).toEqual({});

    // Create
    const resp = await makePostRequest(api.app, "/users", newUser);
    const createdUser = clone(resp.body);

    // Assert resp
    expect(resp.statusCode).toBe(HTTPStatus.OK);
    expect(resp.headers["access-control-allow-origin"]).toBe("*");
    expect(resp.headers["access-control-allow-credentials"]).toBe("true");

    // Get
    const respB = await makeGetRequest(api.app, `/users/${createdUser.id}`);
    expect(respB.headers["access-control-allow-origin"]).toBe("*");
    expect(respB.headers["access-control-allow-credentials"]).toBe("true");
});

function getMockUser() {
    const newUser = {
        first_name: "Manuel",
        last_name: "Rivero",
        email: "mfrr@me.com",
        phone: "7155711524"
    };
    return clone(newUser);
}
