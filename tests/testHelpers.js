import request from "supertest";
import fs from "fs-extra";
import HTTPStatus from "http-status";
import bunyan from "bunyan";

import {
    httpMethods,
    ErrorHelper,
    Controller,
    Application,
    Database,
    Mapper
} from "../lib";

export function makePostRequest(app, url, body) {
    return makeRequest(app, httpMethods.POST, url, body);
}

export function makeGetRequest(app, url) {
    return makeRequest(app, httpMethods.GET, url);
}

export function makePutRequest(app, url, body) {
    return makeRequest(app, httpMethods.PUT, url, body);
}

export function makeDeleteRequest(app, url) {
    return makeRequest(app, httpMethods.DELETE, url);
}

export function makeOptionsRequest(app, url) {
    return makeRequest(app, httpMethods.OPTIONS, url);
}

async function makeRequest(app, method, url, body) {
    const resp = await request(app)
        [method.toString()](url)
        .send(body)
        .set("Accept", "application/json");
    return resp;
}

function getEndpoints() {
    const ctrl = getController();

    // These endpoints need to match what's in swagger
    const endpoints = [
        {
            method: httpMethods.POST,
            path: "/users",
            handler: ctrl.create
        },
        {
            method: httpMethods.GET,
            path: "/users/:id",
            handler: ctrl.get
        },
        {
            method: httpMethods.PUT,
            path: "/users/:id",
            handler: ctrl.update
        },
        {
            method: httpMethods.DELETE,
            path: "/users/:id",
            handler: ctrl.delete
        }
    ];
    return endpoints;
}

function getController() {
    const mapper = new Mapper();
    const database = new Database();
    const application = new Application({
        database
    });

    // Create a new class that throws under certain conditions for testing
    class TestController extends Controller {
        async get(req) {
            const id = this.mapper.getId(req);

            if (id === "random") {
                throw Error("Random error!");
            }

            if (id === "custom") {
                // Don't pass a message here so we test the default message from above
                throw new ErrorHelper.customErrors.CustomError();
            }

            // Trigger response validation error
            if (id === "resp-validation") {
                return {};
            }

            return super.get(req);
        }
    }

    const controller = new TestController({
        application,
        mapper
    });

    return controller;
}

function getCustomErrorDefinitions() {
    const definitions = [
        {
            message: "Path doesn't exist",
            code: "NotFound",
            displayMessage: "You seem to be lost!",
            statusCode: HTTPStatus.NOT_FOUND,
            level: bunyan.WARN
        },
        {
            message: "A custom error",
            code: "Custom",
            displayMessage: "Custom error!",
            statusCode: HTTPStatus.CONFLICT,
            level: bunyan.ERROR
        }
    ];
    return definitions;
}

export async function getDefaultApiConfig() {
    const swagger = await fs.readJson(`${__dirname}/swagger.json`);

    return {
        env: "dev",
        name: "api-test",
        endpoints: getEndpoints(),
        swagger,
        errorDefinitions: getCustomErrorDefinitions(),
        shouldStdOut: true
    };
}

// function createUserHandler(req) {
//     const newPet = req.body;
//     newPet.id = "1"; // TODO: create uuid
//     users[newPet.id] = newPet;
//     return newPet;
// }

// function getUserHandler(req) {
//     const { userId } = req.params;

//     if (userId === "random") {
//         throw Error("Random error!");
//     }

//     if (userId === "custom") {
//         // Don't pass a message here so we test the default message from above
//         throw new customErrors.CustomError();
//     }

//     return users[userId];
// }
