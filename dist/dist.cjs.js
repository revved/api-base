'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var methods = _interopDefault(require('methods'));
var forEach = _interopDefault(require('lodash/forEach'));
var toUpper = _interopDefault(require('lodash/toUpper'));
var HTTPStatus = _interopDefault(require('http-status'));
var get = _interopDefault(require('lodash/get'));
var isNumber = _interopDefault(require('lodash/isNumber'));
var cammelCase = _interopDefault(require('lodash/camelCase'));
var upperFirst = _interopDefault(require('lodash/upperFirst'));
var replace = _interopDefault(require('lodash/replace'));
var bunyan = _interopDefault(require('bunyan'));
var expressOpenapiValidate = require('express-openapi-validate');
var express = _interopDefault(require('express'));
var assignIn = _interopDefault(require('lodash/assignIn'));
var LogzioBunyanStream = _interopDefault(require('logzio-bunyan'));
var PrettyStream = _interopDefault(require('bunyan-prettystream'));
var libSecretManager = require('@revved/lib-secret-manager');
var bodyParser = _interopDefault(require('body-parser'));
var compression = _interopDefault(require('compression'));
var split = _interopDefault(require('lodash/split'));
var startsWith = _interopDefault(require('lodash/startsWith'));
var join = _interopDefault(require('lodash/join'));
var autoBind = _interopDefault(require('auto-bind'));
var merge = _interopDefault(require('lodash/merge'));
var uuid = _interopDefault(require('uuid/v4'));
var moment = _interopDefault(require('moment'));

const httpMethods = {};
forEach(methods, method => {
    httpMethods[toUpper(method)] = method;
});

class ErrorHelper {
    constructor() {
        this.customErrors = {};
    }

    createErrorResponse(error) {
        const errResp = {
            code: error.code || "InternalServer",
            message: error.displayMessage || "Oops! Something went wrong...",
            statusCode: error.statusCode || HTTPStatus.INTERNAL_SERVER_ERROR,
            level: error.level || bunyan.FATAL
        };

        const debugMessage = get(error, "debug.message") || error.message;
        const debugContext = get(error, "debug.context") || {};
        errResp.debug = {
            message: debugMessage,
            context: debugContext
        };

        // Handle speacial case for spec validation error
        // See: https://github.com/Hilzu/express-openapi-validate#class-validationerror-extends-error
        if (error instanceof expressOpenapiValidate.ValidationError) {
            errResp.code = "BadRequestSpecValidation";
            errResp.debug.context = { errors: error.data };
        }

        return errResp;
    }

    defineCustomErrors(errorDefinitions) {
        // Define default errors
        this.defineDefaultErrorDefinitions();

        forEach(errorDefinitions, errorDef => {
            this.defineCustomError(errorDef);
        });
    }

    defineCustomError(errorDef) {
        const code = replace(errorDef.code, "Error", "");
        const className = upperFirst(cammelCase(`${code}Error`));

        this.customErrors[className] = class extends Error {
            constructor(message) {
                super();
                this.message = message || errorDef.message;
                this.code = errorDef.code;
                this.displayMessage = errorDef.displayMessage;
                this.statusCode = errorDef.statusCode;
                this.debug = errorDef.debug;
                this.level = errorDef.level;
            }
        };
    }

    defineDefaultErrorDefinitions() {
        forEach(HTTPStatus, (name, statusCode) => {
            const parsedStatusCode = Number(statusCode);

            if (isNumber(parsedStatusCode) && parsedStatusCode >= 400) {
                const errorDef = {
                    message: HTTPStatus[statusCode],
                    code: upperFirst(cammelCase(name)),
                    displayMessage: HTTPStatus[`${statusCode}_MESSAGE`],
                    statusCode,
                    level: parsedStatusCode < 500 ? bunyan.ERROR : bunyan.FATAL,
                    debug: {}
                };
                this.defineCustomError(errorDef);
            }
        });
    }
}

var ErrorHelper$1 = new ErrorHelper();

let logger;
let config;

// IMPORTANT: this name is use for all envs, but each have their own aws account
const LOGZ_IO_TOKEN_SECRET_MANAGER_KEY = "logz.io_token";

class Logger {
    constructor(configLocal) {
        const loggerOptions = {
            token: configLocal.token
        };

        this.logzioStream = new LogzioBunyanStream(loggerOptions);

        const log = bunyan.createLogger({
            name: configLocal.name,
            serializers: {
                req: bunyan.stdSerializers.req, // TODO: remove auth header
                res: bunyan.stdSerializers.res,
                err: bunyan.stdSerializers.err
            },
            streams: this.getStreams(configLocal)
        });

        // Extend bunyan logger
        const loggerLoc = assignIn(this, log);

        return loggerLoc;
    }

    end() {
        this.logzioStream.end();
    }

    // Uses the error level to log appropietly: .error, vs .info, etc
    logError(error, req, res) {
        try {
            this.logErrorNotCatching(error, req, res);
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }
    }

    logErrorNotCatching(error, req, res) {
        const logEntry = {
            err: error,
            req,
            res
        };

        const level = error.level || bunyan.FATAL;
        const levelName = bunyan.nameFromLevel[level];

        this[levelName](logEntry);
    }

    getStreams(configLocal) {
        const streams = [this.getLogzIoStream()];
        if (configLocal.shouldStdOut) {
            streams.push(this.getStdOutStream());
        }

        return streams;
    }

    getLogzIoStream() {
        return {
            type: "raw",
            level: "warn",
            stream: this.logzioStream
        };
    }

    // eslint-disable-next-line class-methods-use-this
    getStdOutStream() {
        const prettyStdOut = new PrettyStream();
        prettyStdOut.pipe(process.stdout);

        return {
            level: "debug",
            stream: prettyStdOut
        };
    }

    static getInstance() {
        if (!config) {
            throw new Error("Logger.init needs to be called before this");
        }

        if (!logger) {
            logger = new Logger(config);
        }

        return logger;
    }

    static async initConfig(configLocal) {
        config = configLocal;
        const token = await Logger.getLogzIoToken(configLocal.env);
        config.token = token;
    }

    static async getLogzIoToken(env) {
        const token = await libSecretManager.SecretManager.getSecret(
            env,
            LOGZ_IO_TOKEN_SECRET_MANAGER_KEY
        );
        return token;
    }
}

function wrapControllerFn(controllerFn) {
    return async function wrapper(req, res, next) {
        try {
            const response = await controllerFn(req);
            return successHandler(response, req, res, next);
        } catch (error) {
            return errorHandler(error, req, res);
        }
    };
}

function successHandler(response, req, res, next) {
    return res.send(response);
}

function errorHandler(error, req, res, next) {
    const logger = Logger.getInstance();
    logger.logError(error, req, res);

    const errorResponse = ErrorHelper$1.createErrorResponse(error);

    return res.status(errorResponse.statusCode).send(errorResponse);
}

function handleNotFoundRoute(req, res, next) {
    const error = new ErrorHelper$1.customErrors.NotFoundError(
        `Path not found: ${req.path}`
    );
    return errorHandler(error, req, res);
}

async function configureMiddleware(app, opts) {
    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({ extended: true }));

    // parse application/json
    app.use(bodyParser.json());

    // setup compression middleware
    app.use(compression());
}

function configureDefaultErrorHandler(app) {
    app.use(errorHandler);
}

function configureNotFoundEndpoints(app) {
    // All other routes should result in an error
    app.all("*", handleNotFoundRoute);
}

function specValidationMiddlewareForOperation(swagger, method, path) {
    const validator = new expressOpenapiValidate.OpenApiValidator(swagger);
    const newPath = convertColonToCurlyBraces(path);
    const specValMiddleware = validator.validate(method, newPath);

    return specValMiddleware;
}

function convertColonToCurlyBraces(path) {
    const pathParts = split(path, "/");
    const newPathParts = [];

    forEach(pathParts, pathPart => {
        let newPathPart = pathPart;
        if (startsWith(pathPart, ":")) {
            newPathPart = replace(pathPart, ":", "");
            newPathPart = `{${newPathPart}}`;
        }
        newPathParts.push(newPathPart);
    });
    const newPath = join(newPathParts, "/");
    return newPath;
}

class Api {
    constructor() {
        this.app = express();
    }

    async configure(apiConfig) {
        Api.validateConfig(apiConfig);

        this.env = apiConfig.env;
        this.name = apiConfig.name;
        this.port = apiConfig.port;
        this.shouldStdOut = apiConfig.shouldStdOut;

        // Init logger
        await Logger.initConfig({
            env: this.env,
            name: this.name,
            shouldStdOut: this.shouldStdOut
        });

        // Define any custom errors if applicable
        const errorDef = apiConfig.errorDefinitions;
        ErrorHelper$1.defineCustomErrors(errorDef);

        // IMPORTANT: The order here is very important

        // Configgure middleware
        this.configureMiddleware(apiConfig);

        // Register endpoints
        this.registerEndpoints(apiConfig);

        // Configure the default error handler
        configureDefaultErrorHandler(this.app);
    }

    // eslint-disable-next-line class-methods-use-this
    shutDown() {
        const logger = Logger.getInstance();
        logger.end();
    }

    async configureMiddleware(apiConfig) {
        await configureMiddleware(this.app);
    }

    registerEndpoints(apiConfig) {
        const { endpoints } = apiConfig;

        forEach(endpoints, endpoint => {
            this.registerEndpoint(
                apiConfig,
                endpoint.method,
                endpoint.path,
                endpoint.handler
            );
        });

        // All other requests should throw not found errors
        configureNotFoundEndpoints(this.app);
    }

    registerEndpoint(apiConfig, method, path, controllerFn) {
        const wrapper = wrapControllerFn(controllerFn);
        const valMiddleware = specValidationMiddlewareForOperation(
            apiConfig.swagger,
            method,
            path
        );
        this.app[method](path, valMiddleware, wrapper);
    }

    static validateConfig(apiConfig) {
        if (!apiConfig.name) {
            throw new Error("`apiConfig` requires `name` to be passed.");
        }
    }
}

class Controller {
    constructor(options) {
        this.application = options.application;
        this.mapper = options.mapper;

        autoBind(this);
    }

    async get(req) {
        const id = this.mapper.getId(req);
        const resp = await this.application.get(id);
        return resp;
    }

    async create(req) {
        const newObj = this.mapper.getNewObject(req);
        const resp = await this.application.create(newObj);
        return resp;
    }

    async update(req) {
        const id = this.mapper.getId(req);
        const updateObj = this.mapper.getUpdateObject(req);
        const resp = await this.application.update(id, updateObj);
        return resp;
    }

    async delete(req) {
        const id = this.mapper.getId(req);
        const resp = await this.application.delete(id);
        return resp;
    }
}

class Application {
    constructor(options) {
        this.database = options.database;
        autoBind(this);
    }

    async get(id) {
        const objc = this.database.get(id);
        return objc;
    }

    async create(newObj) {
        const createdObj = await this.database.create(newObj);
        return createdObj;
    }

    async update(id, updateObj) {
        const currentObj = await this.database.get(id);
        const updatedObj = merge(currentObj, updateObj);

        await this.database.update(id, updatedObj);
        return updatedObj;
    }

    async delete(id) {
        const deletedObj = await this.database.delete(id);
        return deletedObj;
    }
}

/* eslint-disable no-empty-function */

const store = {};

class Database {
    constructor(options) {
        autoBind(this);
    }

    // Defaults

    async get(id) {
        const obj = store[id];
        if (!obj) {
            console.log("ErrorHelper", ErrorHelper$1.customErrors);

            throw new ErrorHelper$1.customErrors.NotFoundError(
                `User ${id} does not exist`
            );
        }
        return obj;
    }

    async create(newObj) {
        store[newObj.id] = newObj;
        return newObj;
    }

    async update(id, updatedObj) {
        store[id] = updatedObj;
        return updatedObj;
    }

    async delete(id) {
        const currentObj = await this.get(id);
        delete store[id];
        return currentObj;
    }
}

class Mapper {
    constructor() {
        autoBind(this);
    }

    // Get from req

    getId(req) {
        return req.params.id;
    }

    getNewObject(req) {
        const newObj = req.body;
        newObj.id = this.createUuid();
        newObj.created_at = this.getNowTs();
        newObj.updated_at = this.getNowTs();

        return newObj;
    }

    getUpdateObject(req) {
        const updateObj = req.body;
        updateObj.updated_at = this.getNowTs();
        return updateObj;
    }

    // Create data

    createUuid() {
        return uuid();
    }

    getNowTs() {
        return moment().unix();
    }
}

exports.Api = Api;
exports.Application = Application;
exports.Controller = Controller;
exports.Database = Database;
exports.ErrorHelper = ErrorHelper$1;
exports.Logger = Logger;
exports.Mapper = Mapper;
exports.httpMethods = httpMethods;
