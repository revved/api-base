# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.4.0](https://gitlab.com/revved/api-base/compare/v1.3.1...v1.4.0) (2019-08-07)


### Features

* more flexible new object id creation ([2412152](https://gitlab.com/revved/api-base/commit/2412152))



### [1.3.1](https://gitlab.com/revved/api-base/compare/v1.3.0...v1.3.1) (2019-07-31)



## 1.3.0 (2019-07-30)


### Bug Fixes

* Fix watermark for eslintignore ([f88d22a](https://gitlab.com/revved/api-base/commit/f88d22a))
* Fixed issues related to secret manager ([0d092f2](https://gitlab.com/revved/api-base/commit/0d092f2))
* Removed deprecated dep ([8931c5a](https://gitlab.com/revved/api-base/commit/8931c5a))
* renamed test helpers, again ([f381cff](https://gitlab.com/revved/api-base/commit/f381cff))
* Track file rename ([b27af60](https://gitlab.com/revved/api-base/commit/b27af60))


### Features

* added response validation ([3c09546](https://gitlab.com/revved/api-base/commit/3c09546))
* Added rollup config ([2697918](https://gitlab.com/revved/api-base/commit/2697918))
* Better classes ([badd182](https://gitlab.com/revved/api-base/commit/badd182))
* Custom errors ([52a6b5a](https://gitlab.com/revved/api-base/commit/52a6b5a))
* Export bootstraper, and methods ([e7acd63](https://gitlab.com/revved/api-base/commit/e7acd63))
* New version ([b926780](https://gitlab.com/revved/api-base/commit/b926780))
* Swagger validator ([6c398fd](https://gitlab.com/revved/api-base/commit/6c398fd))
* Use CommonError ([a998206](https://gitlab.com/revved/api-base/commit/a998206))
* Use secret manager for logz.io token ([97eb7d9](https://gitlab.com/revved/api-base/commit/97eb7d9))
